package simulation;

import gui.GUI;

public class Clock extends Thread{
	
	private int timer;
	private String c;
	
	public Clock()
	{
		timer=0;
	}
	
	public void run()
	{
		while(Simulare.getStopThreads())
		{
		timer++;
		try {
			c="";
			sleep(1000);
			if(timer>=60)
			{
				if(timer/60>=10)
				{
					c=c+timer/60+":";
				}
				else
				{
					c=c+"0"+timer/60+":";
				}
				if(timer%60 >=10)
				{
					c=c+timer%60;
				}
				else
				{
					c=c+"0"+timer%60;
				}
			}
			else
			{
				c="00:";
				if(timer%60 >=10)
				{
					c=c+timer%60;
				}
				else
				{
					c=c+"0"+timer%60;
				}
			}
			GUI.setTimer(c);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}
	
	public int getTime()
	{
		return timer;
	}
}
