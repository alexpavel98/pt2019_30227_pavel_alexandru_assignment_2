package simulation;
import java.awt.Dimension;
import java.io.IOException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import client.Client;
import gui.GUI;
import queue.*;

public class Simulare extends Thread{

	private int minim_sosire;
	private int maxim_sosire;
	private static int minim_servire;
	private static int maxim_servire;
	private int numar_cozi;
	private static int interval_simulare;
	private static int copie_sim;
	private int avg_asteptare;
	private int avg_service;
	private int avg_empty;
	private String res;
	private int id_client;
	private static List<Coada> cozi;
	private static int max;

	public Simulare()
	{
		cozi=new ArrayList<Coada>();
	}
	
	public void makeQueues()
	{
		max=0;
		for(int i = 0; i < numar_cozi; i++)
		{
			cozi.add(new Coada(i));
			cozi.get(i).start();
		}
	}

	public static void setPeakTime() 
	{
	        int max1=0;
	        for(int i=0;i<cozi.size();i++)
	        {
	        	max1=max1+cozi.get(i).getCoada().size();
	        }
	        if(max1>max)
	        {
	        	max=max1;
	        	Calendar hour = Calendar.getInstance();
		        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		        GUI.setPeakHour("Peak Hour: "+sdf.format((hour).getTime()));
	        }
	}
	
	public static void setAverageWTime()
	{
		int sum=0;
		int nr_elem=0;
		int avg=0;
		for(int i=0;i<cozi.size();i++)
		{
			nr_elem=nr_elem+cozi.get(i).getCoada().size();
		}
		nr_elem=nr_elem-cozi.size();
		List display = new ArrayList(cozi);
		for(int i=0;i<display.size();i++)
		{
			for(int j=1;j<((Coada) display.get(i)).getCoada().size();j++)
			{
				sum=sum+((Client) ((List) ((Coada) display.get(i)).getCoada()).get(j)).gettime();
			}
		}
		if(nr_elem!=0) 
			avg=sum/nr_elem;
		GUI.setAverageWaiting(avg);
	}
	
	public int shortestQueue()
	{
		int min=cozi.get(0).getCoada().size();
		for(int i=0;i<numar_cozi;i++)
		{
			if(cozi.get(i).getCoada().size()<min) min=cozi.get(i).getCoada().size();
		}
		for(int i=0;i<numar_cozi;i++)
		{
			if(cozi.get(i).getCoada().size()==min)
			{
				return i;
			}
		}
		return 0;
	}
	
	public void run()
	{
        try
        {
        	Clock ceas=new Clock();
        	ceas.start();
        	id_client=1;
        	makeQueues();
        	Random sosire=new Random();
        	GUI.setLog("\n\n");
        	copie_sim=interval_simulare;
        	while(interval_simulare>0)
        	{
        		        int nr=sosire.nextInt(maxim_sosire-minim_sosire+1)+minim_sosire;
			            interval_simulare=interval_simulare-nr;
			            if(interval_simulare<0)break;
			            nr=nr*1000;
			            sleep(nr);  
        		        Client c=new Client(id_client);
 			            c.start();
        		        int i=shortestQueue();
			            cozi.get(i).addClient(c);
			            id_client++;
            }
        	GUI.setLog("\n\n\n                    Simularea a luat sfarsit.");
        }
        catch (InterruptedException e) {
					e.printStackTrace();
        }
	}
	
	public static String afisare_cozi()
	{
		String afisare="\n\n COADA 1:           ";
		String warning="\n\n                                          WARNINGS\n\n";
		List display = new ArrayList(cozi);
		for(int i=0;i<display.size();i++)
		{
			for(int j=0;j < ((Coada) display.get(i)).getCoada().size();j++)
			{
				if(((Client) ((List) ((Coada) display.get(i)).getCoada()).get(j)).getStare()==1 || j==0)
				{
				afisare=afisare+"  ( ͡° ͜ʖ ͡°)  ";
				}
				else 
				{
				afisare=afisare+"  ( ¬ _¬)  ";
				warning=warning+"\n\n       Clientul "+((Client) ((List) ((Coada) display.get(i)).getCoada()).get(j)).getIndex()+" asteapta de "+((Client) ((List) ((Coada) display.get(i)).getCoada()).get(j)).gettime()+" secunde!\n\n";
				}
			}
			if(i!=cozi.size()-1) afisare=afisare+"\n\n\n\n COADA "+(i+2)+":           ";
		}
		GUI.setWarning(warning);
		return afisare;
	}
	
	public void setMinSosire(String nr)
	{
		minim_sosire=Integer.parseInt(nr);
	}
	
	public void setMaxSosire(String nr)
	{
		maxim_sosire=Integer.parseInt(nr);
	}
	
	public void setMinimServire(String nr)
	{
		minim_servire=Integer.parseInt(nr);
	}
	
	public void setMaximServire(String nr)
	{
		maxim_servire=Integer.parseInt(nr);
	}
	
	public void setNumarCozi(String nr)
	{
		numar_cozi=Integer.parseInt(nr);
	}
	
	public void setIntervalSimulare(String nr)
	{
		interval_simulare=Integer.parseInt(nr);
	}
	
	public static int getMinimServire()
	{
		return minim_servire;
	}
	
	public static int getMaximServire()
	{
		return maxim_servire;
	}
	
	public static boolean getStopThreads()
	{
		if(interval_simulare <= 0)
			return false;
		return true;
	}
	public static int getIntervalSim()
	{
		return (copie_sim-interval_simulare);
	}
}
