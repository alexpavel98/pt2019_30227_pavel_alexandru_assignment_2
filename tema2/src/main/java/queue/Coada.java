package queue;
import java.awt.Color;
import java.util.*;
import client.*;
import gui.GUI;
import simulation.Simulare;

public class Coada extends Thread{
	
	private Queue<Client> q;
	private int index;
	private String res;
	
	public Coada(int i)
	{
		q = new LinkedList<>();
		index=i;
	}
	
	public Queue<Client> getCoada()
	{
		return q;
	}
	
	synchronized public void addClient(Client c)
	{
		q.add(c);
		Simulare.setPeakTime();
		res=Simulare.afisare_cozi();
	    GUI.setRez(res);
        GUI.setLog("   A sosit clientul "+c.getIndex()+"  la coada "+(index+1)+" la secunda "+Simulare.getIntervalSim()+"\n\n");
        Simulare.setAverageWTime();
   		notifyAll();
	}
	
	synchronized public void sterge_client()
	{
		while(q.size()==0)
		{
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Client c=new Client(0);
		c=q.peek();
		q.remove();
		Simulare.setPeakTime();
		res=Simulare.afisare_cozi();
	    GUI.setRez(res);
		GUI.setLog("   Clientul "+c.getIndex()+" a parasit coada "+(index+1)+" la secunda "+Simulare.getIntervalSim()+"\n\n");
		Simulare.setAverageWTime();
	}
	
	public void run()
	{
		Random sterg =new Random();
		while(true)
		{
			try {
				int nr=sterg.nextInt(Simulare.getMaximServire()-Simulare.getMinimServire()+1)+Simulare.getMinimServire();
				nr=1000*nr;
				sleep(nr);
				if(!Simulare.getStopThreads())break;
				sterge_client();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
