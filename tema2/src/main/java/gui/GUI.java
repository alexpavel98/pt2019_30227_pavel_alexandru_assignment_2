package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import simulation.Simulare;


public class GUI  extends JFrame {
	
	private JButton simulare;
	private JTextField minimArriving,maximArriving,minimService,maximService,numberQueues,simInterval;
	private static TextArea rezultat,evntlog,warnings;
	private JLabel minArriving,maxArriving,minService,maxService,numQueues,sInterval;
	private static JLabel Timer,PeakHour,AverageWaiting;
	public GUI()
	{
		createView();
		setTitle("Simulare cozi clienti");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public void createView()
	{
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setPreferredSize(new Dimension(1920,1080));
		getContentPane().add(panel);
		Timer= new JLabel();
		Timer.setLocation(0, 0);
		Timer.setPreferredSize(new Dimension(700,30));
		Timer.setForeground(Color.darkGray);
		Timer.setFont(Timer.getFont().deriveFont(24.0f));
		panel.add(Timer);
		PeakHour= new JLabel();
		PeakHour.setPreferredSize(new Dimension(350,30));
		PeakHour.setForeground(Color.red);
		PeakHour.setFont(Timer.getFont().deriveFont(14.0f));
		panel.add(PeakHour);
		AverageWaiting= new JLabel();
		AverageWaiting.setPreferredSize(new Dimension(350,30));
		AverageWaiting.setForeground(Color.DARK_GRAY);
		AverageWaiting.setFont(Timer.getFont().deriveFont(14.0f));
		panel.add(AverageWaiting);
		minArriving= new JLabel("Timpul minim sosire: ",SwingConstants.CENTER);
		minArriving.setPreferredSize(new Dimension(500,70));
		panel.add(minArriving);
		minimArriving=new JTextField();
		minimArriving.setPreferredSize(new Dimension (200,50));
		panel.add(minimArriving);
		maxArriving = new JLabel("Timpul maxim sosire: ",SwingConstants.CENTER);
		maxArriving.setPreferredSize(new Dimension(500,70));
		panel.add(maxArriving);
		maximArriving=new JTextField();
		maximArriving.setPreferredSize(new Dimension (200,50));
		panel.add(maximArriving);
		minService = new JLabel("Timpul minim de servire: ",SwingConstants.CENTER);
		minService.setPreferredSize(new Dimension(500,70));
		panel.add(minService);
		minimService=new JTextField();
		minimService.setPreferredSize(new Dimension (200,50));
		panel.add(minimService);
		maxService = new JLabel("Timpul maxim de servire: ",SwingConstants.CENTER);
		maxService.setPreferredSize(new Dimension(500,70));
		panel.add(maxService);
		maximService=new JTextField();
		maximService.setPreferredSize(new Dimension (200,50));
		panel.add(maximService);
		numQueues = new JLabel("Numar de cozi: ",SwingConstants.CENTER);
		numQueues.setPreferredSize(new Dimension(500,70));
		panel.add(numQueues);
		numberQueues=new JTextField();
		numberQueues.setPreferredSize(new Dimension (200,50));
		panel.add(numberQueues);
		sInterval = new JLabel("Interval Simulare: ",SwingConstants.CENTER);
		sInterval.setPreferredSize(new Dimension(500,70));
		panel.add(sInterval);
		simInterval=new JTextField();
		simInterval.setPreferredSize(new Dimension (200,50));
		panel.add(simInterval);
		simulare= new JButton();
		simulare.setPreferredSize(new Dimension(1200,50));
		simulare.setText("SIMULARE");
		simulare.addActionListener(new Buton());
		panel.add(simulare);
		evntlog=new TextArea();
		evntlog.setPreferredSize(new Dimension(350,450));
		evntlog.setBackground(Color.blue);
		evntlog.setForeground(Color.white);
		panel.add(evntlog);
		rezultat=new TextArea();
		rezultat.setPreferredSize(new Dimension(700,450));
		rezultat.setBackground(Color.LIGHT_GRAY);
		panel.add(rezultat);
		warnings=new TextArea();
		warnings.setPreferredSize(new Dimension(350,450));
		warnings.setBackground(Color.black);
		warnings.setForeground(Color.RED);
	    panel.add(warnings);
	}
	
	public static void setRez(String res)
	{
		rezultat.setText(res);
	}
	
	public static void setLog(String res)
	{
		evntlog.append(res);
	}
	
	public static void setWarning(String res)
	{
		warnings.setText(res);
	}
	
	public static void setTimer(String time)
	{
		Timer.setText(time);
	}
	
	public static void setPeakHour(String time)
	{
		PeakHour.setText(time);
	}
	
	public static void setAverageWaiting(int time)
	{
		AverageWaiting.setText("Average Waiting Time: "+Integer.toString(time));
	}
	private class Buton implements ActionListener
	{
		Simulare sim= new Simulare();
		public void actionPerformed(ActionEvent e) {
			try
			{
			sim.setMinSosire(minimArriving.getText());
			sim.setMaxSosire(maximArriving.getText());
			sim.setMinimServire(minimService.getText());
			sim.setMaximServire(maximService.getText());
			sim.setNumarCozi(numberQueues.getText());
			sim.setIntervalSimulare(simInterval.getText());
			sim.start();
			}
			catch(NumberFormatException f)
			{
				rezultat.setText("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n                                                                                             Eroare de introducere!");
			}	
				
			
		}
		
	}

}