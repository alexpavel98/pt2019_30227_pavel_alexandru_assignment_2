package client;

import java.util.Random;

import simulation.Simulare;

public class Client extends Thread{

	private int Stare;
	private int time;
	private int index;
	
	public Client(int i) {
		Stare=1;
		time=0;
		index=i;
	};
	
	public void setStare()
	{
		if(time>=30)
		{
		    Stare=0;
		}
	}
	
	public int getStare()
	{
		return Stare;
	}
	
	public int gettime()
	{
		return time;
	}
	
	public int getIndex()
	{
		return index;
	}
	
	public void run()
	{
		while(true)
		{
		try {
			time++;
			sleep(1000);
			setStare();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(!Simulare.getStopThreads())break;
		}
	}
}

